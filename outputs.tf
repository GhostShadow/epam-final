output "bastion_ip" {
  value = "${module.bastion.bastion_ip}"
}

output "balancer_address" {
  value = "${module.loadbalancer.loadbalancer_address}"
}
