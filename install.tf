module "vpc" {
  source = "modules/vpc"
  AZNum  = "${var.AvailabilityZonesNum}"
  region = "${var.region}"
}

module "security" {
  source = "modules/security"

  vpc_id = "${module.vpc.vpc_id}"
}

module "s3" {
  source = "modules/s3"

  allowed_role_arn = "${module.security.app_role_arn}"
}

module "bastion" {
  source            = "modules/bastion"
  subnet_id         = "${module.vpc.public_subnets_id[0]}"
  ami_id            = "${var.bastion_ami_id}"
  instance_type     = "${var.bastion_type}"
  key_id            = "${var.ssh_key_name}"
  security_group_id = "${module.security.bastion_id}"
}

module "loadbalancer" {
  source            = "modules/loadbalancer"
  loadBalancerSG    = "${module.security.loadbalancer_id}"
  public_subnets_id = "${module.vpc.public_subnets_id}"
  vpc_id            = "${module.vpc.vpc_id}"
  target_group      = "${module.autoscaling.app_autoscaling_group_id}"
}

module "autoscaling" {
  source                  = "modules/autoscaling"
  image_id                = "${var.image_id}"
  desired_size            = "${var.autoscaling_desired_size}"
  key_id                  = "${var.ssh_key_name}"
  instance_type           = "${var.instance_type}"
  security_group_id       = "${module.security.autoscaling_id}"
  private_subnets_id      = "${module.vpc.private_subnets_id}"
  iam_instance_profile_id = "${module.security.iam_instance_profile_id}"
  loadbalancer_arn        = "${module.loadbalancer.loadbalancer_arn}"
  requests_per_node       = "${var.requests_per_node}"
  loadbalancer_arn_suffix = "${module.loadbalancer.loadbalancer_arn_suffix}"
}
