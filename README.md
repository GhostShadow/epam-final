## This is a test application to show how terraform can be used.

The setup consists of loadbalancer that forwards traffic to an autoscaling group.

The nodes in the autoscaling group are set up with ansible. Ansible files are stored in S3 bucket in zip archive. This archive contains the content of folder resources/deploy and the zip file config.zip needs to be recreated and reuploaded if the changes are made to configuration. If you would like use some other archive you'll need to modify modules S3 and Autoscaling(change file name uploaded to S3 and userdata command that extracts the content on the node during the bootstrap process)

The application (index.html) is in ansible app role. Static files can be found in resources/static folder. If you have lots of files you can use https://github.com/saymedia/terraform-s3-dir

The output of Terraform apply will have the bastion IP address you can use to access the nodes in autoscaling group and the loadbalancer URL you can use to access the application.

Application will be available till **20.03.2018**. After that on demand.

### Setup steps:
- If there were changes if resources/deploy recreate config.zip file
- Make needed changes to variables.tf
- Run aws configure with credentials with sufficent rights
- Run terraform init
- Run terraform apply


### Variables description:

- **"image_id"** -  Ami image id for nodes. Currently all scripts are made to work with ubuntu, so you might have to change user data and
   some ansible scripts in case you would like to use something else
- **"AvailabilityZonesNum"** - Number of availability zones used. Defaults to 2
- **"region"** - AWS region to deploy application
- **"ssh\_key\_name"** - SSH key name
- **"autoscaling\_desired\_size"** - Desired size of autoscaling group. Defaults to 2
- **"instance_type"** - Instance type for autoscaling group
- **"requests\_per\_node"** - Number of requests per second from the loadbalancer per node. Uses in autoscaling group policy to scale the
   number of nodes
- **"bastion\_ami\_id"** - Ami id for bastion instance
- **"bastion_type"** - Instance type for bastion instance