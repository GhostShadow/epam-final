output "loadbalancer_id" {
  value = "${aws_security_group.loadBalancerSG.id}"
}

output "autoscaling_id" {
  value = "${aws_security_group.autoscalingSG.id}"
}

output "bastion_id" {
  value = "${aws_security_group.bastion.id}"
}

output "app_role_arn" {
  value = "${aws_iam_role.app.arn}"
}

output "iam_instance_profile_id" {
  value = "${aws_iam_instance_profile.app.id}"
}
