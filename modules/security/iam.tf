resource "aws_iam_role" "app" {
  name = "application_role"

  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
        {
        "Action": "sts:AssumeRole",
        "Principal": {
            "Service": "ec2.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
        }
    ]
    }
    EOF
}

resource "aws_iam_instance_profile" "app" {
  name = "app"
  role = "${aws_iam_role.app.name}"
}

resource "aws_iam_policy" "apppolicy" {
  name        = "appPolicy"
  description = "Application access policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:List*",
        "s3:Get*",
        "s3:Put"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = "${aws_iam_role.app.name}"
  policy_arn = "${aws_iam_policy.apppolicy.arn}"
}
