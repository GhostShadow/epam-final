resource "aws_s3_bucket" "static" {
  bucket = "shedtest-app-static"
  acl    = "private"
}

resource "aws_s3_bucket_policy" "static" {
  bucket = "${aws_s3_bucket.static.id}"

  policy = <<POLICY
{
        "Version": "2012-10-17",
        "Id": "StaticBuckerPolicy",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal" : {"AWS":"*"},
                "Action": ["s3:GetObject","s3:ListBucket"],
                "Resource": ["arn:aws:s3:::shedtest-app-static/*","arn:aws:s3:::shedtest-app-static"]
            }
        ]
    }
    POLICY
}

resource "aws_s3_bucket" "config" {
  bucket = "shedtest-app-config"
  acl    = "private"
}

resource "aws_s3_bucket_policy" "config" {
  bucket = "${aws_s3_bucket.config.id}"

  policy = <<POLICY
{
        "Version": "2012-10-17",
        "Id": "StaticBuckerPolicy",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal" : {"AWS":["${var.allowed_role_arn}"]},
                "Action": ["s3:GetObject","s3:ListBucket"],
                "Resource": ["arn:aws:s3:::shedtest-app-config/*","arn:aws:s3:::shedtest-app-config"]
            }
        ]
    }
    POLICY
}
