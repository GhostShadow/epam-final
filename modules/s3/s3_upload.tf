resource "aws_s3_bucket_object" "config" {
  bucket     = "shedtest-app-config"
  key        = "config.zip"
  source     = "resources/config.zip"
  etag       = "${md5(file("resources/config.zip"))}"
  depends_on = ["aws_s3_bucket.config"]
}

resource "aws_s3_bucket_object" "drag1" {
  bucket     = "shedtest-app-static"
  key        = "drag1.jpg"
  source     = "resources/static/drag1.jpg"
  etag       = "${md5(file("resources/static/drag1.jpg"))}"
  depends_on = ["aws_s3_bucket.static"]
}

resource "aws_s3_bucket_object" "drag2" {
  bucket     = "shedtest-app-static"
  key        = "drag2.jpg"
  source     = "resources/static/drag2.jpg"
  etag       = "${md5(file("resources/static/drag2.jpg"))}"
  depends_on = ["aws_s3_bucket.static"]
}

resource "aws_s3_bucket_object" "drag3" {
  bucket     = "shedtest-app-static"
  key        = "drag3.jpg"
  source     = "resources/static/drag3.jpg"
  etag       = "${md5(file("resources/static/drag3.jpg"))}"
  depends_on = ["aws_s3_bucket.static"]
}

resource "aws_s3_bucket_object" "drag4" {
  bucket     = "shedtest-app-static"
  key        = "drag4.jpg"
  source     = "resources/static/drag4.jpg"
  etag       = "${md5(file("resources/static/drag4.jpg"))}"
  depends_on = ["aws_s3_bucket.static"]
}
