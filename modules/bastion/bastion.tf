resource "aws_instance" "bastion" {
  ami                         = "${var.ami_id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_id}"
  subnet_id                   = "${var.subnet_id}"
  security_groups             = ["${var.security_group_id}"]
  associate_public_ip_address = "1"

  tags {
    Name = "AppBastion"
  }
}
