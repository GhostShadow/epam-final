resource "aws_internet_gateway" "app_igw" {
  vpc_id = "${aws_vpc.app_vpc.id}"

  tags {
    Name = "IGW"
  }
}

resource "aws_eip" "nat_ip" {
  count = "${var.AZNum}"
  vpc   = true

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_nat_gateway" "app_nat" {
  count = "${var.AZNum}"

  allocation_id = "${element(aws_eip.nat_ip.*.id, count.index)}"
  subnet_id     = "${element(aws_subnet.app_public_subnet.*.id, count.index)}"

  lifecycle {
    create_before_destroy = true
  }
}
