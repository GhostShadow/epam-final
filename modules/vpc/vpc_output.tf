output "vpc_id" {
  value = "${aws_vpc.app_vpc.id}"
}

output "private_subnets_id" {
  value = "${aws_subnet.app_private_subnet.*.id}"
}

output "public_subnets_id" {
  value = "${aws_subnet.app_public_subnet.*.id}"
}
