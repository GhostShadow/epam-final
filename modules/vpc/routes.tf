resource "aws_route_table" "public_route_table" {
  vpc_id = "${aws_vpc.app_vpc.id}"

  tags {
    Name = "RouteTablePublic"
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = "${aws_vpc.app_vpc.id}"
  count  = "${var.AZNum}"

  tags {
    Name = "${join("-","${list( "PrivateRouteTable","${count.index}")}" )}"
  }
}

resource "aws_route" "public_route_default" {
  route_table_id         = "${aws_route_table.public_route_table.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.app_igw.id}"
}

resource "aws_route" "private_route_default" {
  count = "${var.AZNum}"

  route_table_id         = "${element(aws_route_table.private_route_table.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${element(aws_nat_gateway.app_nat.*.id, count.index)}"
}

resource "aws_route_table_association" "private_route_table_assoc" {
  count = "${var.AZNum}"

  subnet_id      = "${element(aws_subnet.app_private_subnet.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private_route_table.*.id, count.index)}"
}

resource "aws_route_table_association" "public_route_table_assoc" {
  count          = "${var.AZNum}"
  subnet_id      = "${element(aws_subnet.app_public_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.public_route_table.id}"
}

resource "aws_vpc_endpoint" "env_s3_endpoint" {
  vpc_id       = "${aws_vpc.app_vpc.id}"
  service_name = "${join(".","${list("com.amazonaws", var.region, "s3")}")}"

  route_table_ids = [
    "${aws_route_table.public_route_table.id}",
    "${aws_route_table.private_route_table.*.id}",
  ]
}
