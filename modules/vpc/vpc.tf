resource "aws_vpc" "app_vpc" {
  cidr_block         = "10.0.0.0/16"
  enable_dns_support = true

  tags {
    Name = "AppVPC"
  }
}

data "aws_availability_zones" "env_available_azs" {}

resource "aws_subnet" "app_private_subnet" {
  count             = "${var.AZNum}"
  vpc_id            = "${aws_vpc.app_vpc.id}"
  cidr_block        = "${join("","${list("10.0.","${count.index}", ".0/24")}")}"
  availability_zone = "${data.aws_availability_zones.env_available_azs.names[count.index]}"

  tags {
    Name = "${join("-","${list( "PrivateSubnet","${count.index}")}" )}"
  }

  lifecycle {
    create_before_destroy = false
  }
}

resource "aws_subnet" "app_public_subnet" {
  count             = "${var.AZNum}"
  vpc_id            = "${aws_vpc.app_vpc.id}"
  cidr_block        = "${join("","${list("10.0.1","${count.index}", ".0/24")}")}"
  availability_zone = "${data.aws_availability_zones.env_available_azs.names[count.index]}"

  tags {
    Name = "${join("-","${list( "PublicSubnet","${count.index}")}" )}"
  }

  lifecycle {
    create_before_destroy = false
  }
}

resource "aws_default_network_acl" "env_network_acl" {
  default_network_acl_id = "${aws_vpc.app_vpc.default_network_acl_id}"

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags {
    Name = "ACL"
  }

  subnet_ids = [
    "${aws_subnet.app_public_subnet.*.id}",
    "${aws_subnet.app_private_subnet.*.id}",
  ]
}
