output "loadbalancer_arn" {
  value = "${aws_lb_target_group.application.arn}"
}

output "loadbalancer_arn_suffix" {
  value = "${aws_lb.loadBalancer.arn_suffix}/${aws_lb_target_group.application.arn_suffix}"
}

output "loadbalancer_address" {
  value = "${aws_lb.loadBalancer.dns_name}"
}
