resource "aws_lb" "loadBalancer" {
  name            = "test-lb"
  security_groups = ["${var.loadBalancerSG}"]
  subnets         = ["${var.public_subnets_id}"]

  tags {
    Name = "LB"
  }
}

resource "aws_lb_target_group" "application" {
  name        = "application-tg"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = "${var.vpc_id}"
  target_type = "instance"

  health_check {
    interval            = "5"
    healthy_threshold   = "2"
    unhealthy_threshold = "2"
    timeout             = "2"
    protocol            = "HTTP"
    path                = "/index.html"
    port                = "8080"
  }
}

resource "aws_lb_listener" "application" {
  load_balancer_arn = "${aws_lb.loadBalancer.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.application.arn}"
    type             = "forward"
  }
}
