#!/bin/bash
sudo add-apt-repository -y ppa:ansible/ansible
sudo apt update
sudo apt install -y awscli ansible unzip
aws s3 cp s3://shedtest-app-config/config.zip config.zip 
unzip config.zip
HOME=/root ansible-playbook deploy/install.yml