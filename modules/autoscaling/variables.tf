variable image_id {}
variable desired_size {}
variable key_id {}
variable instance_type {}
variable iam_instance_profile_id {}
variable security_group_id {}
variable loadbalancer_arn {}
variable loadbalancer_arn_suffix {}
variable requests_per_node {}

variable private_subnets_id {
  type = "list"
}
