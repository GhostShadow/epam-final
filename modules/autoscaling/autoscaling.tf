resource "aws_launch_configuration" "launch_conf" {
  name_prefix          = "app-"
  image_id             = "${var.image_id}"
  instance_type        = "${var.instance_type}"
  user_data            = "${template_file.user_data.rendered}"
  iam_instance_profile = "${var.iam_instance_profile_id}"
  security_groups      = ["${var.security_group_id}"]
  key_name             = "${var.key_id}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "app" {
  name                 = "app"
  launch_configuration = "${aws_launch_configuration.launch_conf.name}"
  vpc_zone_identifier  = ["${var.private_subnets_id}"]
  min_size             = 1
  max_size             = "${var.desired_size}"
  target_group_arns    = ["${var.loadbalancer_arn}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "template_file" "user_data" {
  template = "${file("${path.module}/userdata.tpl")}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "app" {
  name                   = "app_scaling_requests_per_node"
  policy_type            = "TargetTrackingScaling"
  autoscaling_group_name = "${aws_autoscaling_group.app.name}"

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ALBRequestCountPerTarget"
      resource_label         = "${var.loadbalancer_arn_suffix}"
    }

    target_value = "${var.requests_per_node}"
  }
}
