variable "image_id" {
  default = "ami-66506c1c"
}

variable "AvailabilityZonesNum" {
  type    = "string"
  default = "2"
}

variable "region" {
  type    = "string"
  default = "us-east-1"
}

variable "ssh_key_name" {
  type    = "string"
  default = "home_key"
}

variable "autoscaling_desired_size" {
  default = "2"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "requests_per_node" {
  default = "10"
}

variable "bastion_ami_id" {
  default = "ami-66506c1c"
}

variable "bastion_type" {
  default = "t2.micro"
}
